<!DOCTYPE html>
<html lang="en-US">
    <head>
    <title>CampGemini: ${item.name}</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="Keywords" content="Capgemini,Testing,API-Testing,API">
    <meta name="Description" content="CampGemini application to practice API testing.">
    <link href='https://fonts.googleapis.com/css?family=Ubuntu' rel='stylesheet'>
    <!-- Bootstrap, jQuery, Popper en JS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    </head>
   <body>
      <div class="jumbotron">
       <h1>CampGemini</h1>
       <p>For your life with or without IT</p>
      </div>
      <div class="row container">
       <div class="col">
           <div class="card">
               <div class="card-header">
                   <p><h3>${item.name}</h3></p>
               </div>
               <div class="card-body">
                   <h4>Price</h4>
                   <p>${item.price}</p>
                   <h4>Supply</h4>
                   <p>${item.supply}</p>
                   <h4>Description</h4>
                   <p>${item.description}</p>
               </div>
               <div class="card-footer">
                   <p><a href="/catalogue/">Back</a></p>
               </div>
           </div>
       </div>
      </div>
   </body>
</html>