package nl.capgemini.testing.campgemini.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.HandlerInterceptor;

import java.util.Enumeration;

@Component
public class CrudApiInterceptor implements HandlerInterceptor {

    private static Logger LOGGER = LoggerFactory.getLogger(CrudApiInterceptor.class);

    @Override
    public boolean preHandle(
            HttpServletRequest request,
            HttpServletResponse response,
            Object handler) {
        LOGGER.info(formatRequest(request));
        return true;
    }

    private String formatRequest(HttpServletRequest request) {
        Enumeration<String > myHeaders = request.getHeaderNames();
        String logline = "\n{\"Headers\":\n {\n";
        while(myHeaders.hasMoreElements()) {
            String header = myHeaders.nextElement();
            logline += "\""+header+"\" :" +"\""+request.getHeader(header)+"\"" +"\n";

        }
        logline += "}\n";
        logline += "\"Method\": " +"\""+request.getMethod() +"\"\n}";
        return logline;
    }


}
