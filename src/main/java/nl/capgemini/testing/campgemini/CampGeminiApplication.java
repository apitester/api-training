package nl.capgemini.testing.campgemini;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan("nl.capgemini.testing.campgemini")
public class CampGeminiApplication {

    public static void main(String[] args) {
        new SpringApplicationBuilder(CampGeminiApplication.class)
                .headless(false)
                .run(args);
    }

}
