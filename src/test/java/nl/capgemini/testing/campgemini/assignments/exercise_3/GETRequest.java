package nl.capgemini.testing.campgemini.assignments.exercise_3;

import org.junit.jupiter.api.Test;
import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


class GETRequest {

    @Test
    void GetRequestTest()
    {
        given().
                baseUri("http://localhost:8080/").
        when().
                get("api/campgemini/")
        ;
    }
}

