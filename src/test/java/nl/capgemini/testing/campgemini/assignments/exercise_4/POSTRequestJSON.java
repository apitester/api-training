package nl.capgemini.testing.campgemini.assignments.exercise_4;

import com.fasterxml.jackson.databind.JsonNode;
import io.restassured.http.ContentType;
import io.restassured.internal.common.assertion.Assertion;
import nl.capgemini.testing.campgemini.helpers.JSONfileReader;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.ArrayList;

import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.*;

class POSTRequestJSON {

    private JSONfileReader jsonFileReader = JSONfileReader.getInstance();
    private ArrayList<JsonNode> jsonTestDataList;

    /**
     * This method will read before each test our Testdata located in src/test/resources.testData.json
     * If you want to get the first item of our test data you can use "jsonTestDataList.get(0)";
     * jsonTestDataList.get(0).toString(); Convert the data into a String object that can be Posted to our backend.
     */

    String baseUri ="http://localhost:8080/api/campgemini/";

    @BeforeEach public void
    readData() throws IOException {
        String path = "src/test/resources/testData.json";
        jsonTestDataList = jsonFileReader.getJsonArray(path);
    }

    @Test void
    postWithJson() {

        {
            given().log().all().header("role","employee")
                    .baseUri(baseUri).
            when().
                    contentType(ContentType.JSON).
                    body("Put something here to retrieve from jsonTestDataList").post().
            then().
                    statusCode(201);
        }
    }
}
